﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Memory{
	public string Name;
	public float Value;

	public Memory(string name, float value)
	{
		Name = name;
		Value = value;
	}

}
