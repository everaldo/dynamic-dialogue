﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : WorldObject {
	public StoredContent Memories;

	public Dictionary<string, float> Stats;

	void Start()
	{
		_HashedName = Utility.GetUniqueSymbol (Name);
		Stats = new Dictionary<string, float> ();
		EventManager.Instance.AddListener<AskResponseEvent> (GiveResponse);
	}

	override public List<Memory> GetInfo()
	{
		List<Memory> objectMemory = new List<Memory> ();
		objectMemory.Add (new Memory("Who", _HashedName));

		foreach (KeyValuePair<string,float> keyValue in Memories.Memories) 
			objectMemory.Add (new Memory(keyValue.Key, keyValue.Value));

		foreach (KeyValuePair<string,float> statValue in Stats) 
			objectMemory.Add (new Memory(statValue.Key, statValue.Value));

		return objectMemory;
	}

	void GiveResponse(AskResponseEvent e)
	{
		if (e.Who == this.Name) 
		{
			ResponsiveQuery query = new ResponsiveQuery ();

			List<Memory> memories = GetInfo ();

			foreach (Memory memory in memories)
				query.Add (memory.Name, memory.Value);

			query.Add ("Concept", e.Concept);

			query.Execute ();
		}
	}
}
