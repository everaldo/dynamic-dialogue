﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerListener : MonoBehaviour {
	private WorldObject _Character;
	private StoredContent _CharacterMemories;

	void Start()
	{
		_Character = GetComponentInParent<WorldObject> ();
		_CharacterMemories = GetComponentInParent<StoredContent> ();
	}

	void OnTriggerEnter(Collider c)
	{
		if (c.tag == "WorldObject" && c.gameObject.layer == LayerMask.NameToLayer ("Room")) 
		{
			ResponsiveQuery query = new ResponsiveQuery ();

			WorldObject worldObject = c.GetComponent<WorldObject>();

			_CharacterMemories.NewMemory ("Inside", worldObject._HashedName);

			List<Memory> memories = _Character.GetInfo ();
			List<Memory> seenObjectInfo = worldObject.GetInfo ();

			foreach (Memory memory in memories)
				query.Add (memory.Name, memory.Value);
			foreach (Memory memory in seenObjectInfo)
				query.Add (memory.Name, memory.Value);

			query.Add ("Concept", Utility.GetUniqueSymbol ("Enter"));

			query.Execute ();
		}
	}

	void OnTriggerExit(Collider c)
	{
		if (c.tag == "WorldObject" && c.gameObject.layer == LayerMask.NameToLayer ("Room")) 
		{
			ResponsiveQuery query = new ResponsiveQuery ();

			WorldObject worldObject = c.GetComponent<WorldObject>();

			_CharacterMemories.RemoveMemory ("Inside", worldObject._HashedName);

			List<Memory> memories = _Character.GetInfo ();
			List<Memory> seenObjectInfo = worldObject.GetInfo ();

			foreach (Memory memory in memories)
				query.Add (memory.Name, memory.Value);
			foreach (Memory memory in seenObjectInfo)
				query.Add (memory.Name, memory.Value);

			query.Add ("Concept", Utility.GetUniqueSymbol ("Exit"));

			query.Execute ();
		}
	}
}
