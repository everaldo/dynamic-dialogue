﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldObject : MonoBehaviour {
	public string Name;
	[HideInInspector]
	public float _HashedName;

	void Start()
	{
		_HashedName = Utility.GetUniqueSymbol (Name);
	}

	virtual public List<Memory> GetInfo()
	{
		List<Memory> objectMemory = new List<Memory> ();
		objectMemory.Add (new Memory("Who", _HashedName));
		return objectMemory;
	}
}
