﻿#define DEBUG

using UnityEngine;
using System.Collections;

public class Logger 
{
    public static void Log(string log)
    {
#if DEBUG
        Debug.Log(log);
#endif
    }

    public static void LogWarning(string log)
    {
#if DEBUG
        Debug.LogWarning(log);
#endif
    }

    public static void LogError(string log)
    {
#if DEBUG
        Debug.LogError(log);
#endif
    }
}
