﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PresenceListener : MonoBehaviour {
	private WorldObject _Character;
	private StoredContent _CharacterMemories;

	void Start()
	{
		_Character = GetComponentInParent<WorldObject> ();
		_CharacterMemories = GetComponentInParent<StoredContent> ();
	}

	void OnTriggerEnter(Collider c)
	{
		if (c.gameObject != _Character.gameObject && c.tag == "WorldObject" && c.gameObject.layer == LayerMask.NameToLayer("Character")) 
		{
			ResponsiveQuery query = new ResponsiveQuery ();

			WorldObject worldObject = c.GetComponent<WorldObject>();

			_CharacterMemories.NewMemory (worldObject.Name+"Nearby", 1f);

			List<Memory> memories = _Character.GetInfo ();

			foreach (Memory memory in memories)
				query.Add (memory.Name, memory.Value);
			
			query.Add ("Concept", Utility.GetUniqueSymbol ("SomeoneNearby"));

			query.Execute ();
		}
	}

	void OnTriggerExit(Collider c)
	{
		if (c.gameObject != _Character.gameObject &&  c.tag == "WorldObject" && c.gameObject.layer == LayerMask.NameToLayer ("Character")) 
		{
			ResponsiveQuery query = new ResponsiveQuery ();

			WorldObject worldObject = c.GetComponent<WorldObject>();

			_CharacterMemories.NewMemory (worldObject.Name+"Nearby", 0);

			List<Memory> memories = _Character.GetInfo ();

			foreach (Memory memory in memories)
				query.Add (memory.Name, memory.Value);

			query.Add ("Concept", Utility.GetUniqueSymbol ("SomeoneAway"));

			query.Execute ();
		}
	}
}
