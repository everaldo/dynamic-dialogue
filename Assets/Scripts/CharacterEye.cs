﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CharacterEye : MonoBehaviour {

	private WorldObject _Character;

	void Start()
	{
		_Character = GetComponentInParent<WorldObject> ();
	}

	void OnTriggerEnter(Collider c)
	{
		if (c.gameObject != _Character.gameObject && c.tag == "WorldObject") 
		{
			ResponsiveQuery query = new ResponsiveQuery ();

			WorldObject worldObject = c.GetComponent<WorldObject>();

			List<Memory> memories = _Character.GetInfo ();

			foreach (Memory memory in memories)
				query.Add (memory.Name, memory.Value);

			query.Add ("What", worldObject._HashedName);
			query.Add ("Concept", Utility.GetUniqueSymbol ("OnSee"));

			query.Execute ();
		}
	}
}
