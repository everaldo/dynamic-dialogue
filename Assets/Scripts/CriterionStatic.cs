﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CriterionStatic{
	[Header("Criterion adalah key")]
	public string Criterion;
	[Header("Fa dan Fb adalah value, gunakan rule IEEE754; Valve said string value is for noob")]
	public float Fa;
	public float Fb;

	public bool Compare(float x)
	{
		return Fa <= x && x <= Fb; 
	}
}
