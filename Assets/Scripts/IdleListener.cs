﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections.Generic;

public class IdleListener : MonoBehaviour {
	
	public float WaitTime = 4f;
	private IEnumerator _Enumerator;
	private WorldObject _Character;
	private StoredContent PlayerMemories;

	// Use this for initialization
	void Start () {
		_Enumerator = IdleCounter ();

		_Character = GetComponent<WorldObject> ();
		PlayerMemories = GetComponent<StoredContent> ();
		StartCoroutine (_Enumerator);
	}
	
	// Update is called once per frame
	void Update () {
		float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
		float vertical = CrossPlatformInputManager.GetAxis("Vertical");
		if (horizontal != 0f || vertical != 0f) 
		{
			PlayerMemories.NewMemory ("IsIdle", 0f);
			StopCoroutine (_Enumerator);
			_Enumerator = IdleCounter ();
			StartCoroutine (_Enumerator);
		}
	}

	IEnumerator IdleCounter()
	{
		yield return new WaitForSeconds (WaitTime);
		ResponsiveQuery query = new ResponsiveQuery ();

		PlayerMemories.NewMemory ("IsIdle", 1f);

		List<Memory> memories = _Character.GetInfo ();

		foreach (Memory memory in memories)
			query.Add (memory.Name, memory.Value);

		query.Add ("Concept", Utility.GetUniqueSymbol ("OnIdle"));

		query.Execute ();
	}
}
