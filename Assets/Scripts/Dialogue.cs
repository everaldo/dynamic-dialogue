﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour {

	public static Dialogue _Instance;

	public Image Canvas;
	public Text DialogueText; 

	private IEnumerator _Enumerator;

	public List<NameColor> NameColor; 
	private Dictionary<float,Color> ColorDictionary;

	void Awake()
	{
		_Instance = this;
		ColorDictionary = new Dictionary<float, Color> ();
		foreach (NameColor nameColor in NameColor) 
			ColorDictionary.Add(Utility.GetUniqueSymbol(nameColor.Name), nameColor.Color);
	}

	public void PlayDialogue(float Who,string DialogueName, string whoNextResponse, float nextConceptResponse)
	{
		_Enumerator = PlayingDialogue (DialogueName, ColorDictionary[Who],whoNextResponse, nextConceptResponse);
		StartCoroutine (_Enumerator);
	}

	IEnumerator PlayingDialogue(string Text, Color c,string whoNextResponse, float nextConceptResponse)
	{
		DialogueText.text = Text;
		Canvas.color = c;
		Canvas.gameObject.SetActive (true);
		yield return new WaitForSeconds (2f);
		Canvas.gameObject.SetActive (false);
		if (whoNextResponse != "")
			EventManager.Instance.TriggerEvent (new AskResponseEvent (whoNextResponse, nextConceptResponse));
	}
}

[System.Serializable]
public class NameColor
{
	public string Name;
	public Color Color;
}