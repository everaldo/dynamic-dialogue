﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResponseDatabase : MonoBehaviour
{
	public List<Response> Responses;

	void Start()
	{
		EventManager.Instance.AddListener<SearchResponseEvent> (SearchResponse);		
	}

	void SearchResponse(SearchResponseEvent e)
	{
		foreach(Response resp in Responses)
		{
			if (resp.Name == e.ResponseName) 
			{
				resp.GiveResponse (e.WhoTalk);
				return;
			}
		}
	}
}

[System.Serializable]
public class Response{
	public string Name;
	public List<DialogueScene> DialogueScene;

	public void GiveResponse(float whoTalk)
	{
		Random rnd = new Random ();
		DialogueScene [Random.Range (0, DialogueScene.Count)].ResponseDialogue(whoTalk);
	}

}

[System.Serializable]
public class DialogueScene
{
	public string DialogueID;
	public string WhoNextResponse;
	public string Concept;

	public void ResponseDialogue(float whoTalk)
	{
		Dialogue._Instance.PlayDialogue (whoTalk, DialogueID, WhoNextResponse, Utility.GetUniqueSymbol(Concept));
	}
}
