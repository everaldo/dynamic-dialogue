﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResponsiveQuery{
	private Dictionary<string, float> _Query;

	public ResponsiveQuery()
	{
		_Query = new Dictionary<string, float> ();
	}

	public void Add(string key, float x)
	{
		_Query.Add (key, x);
	}

	public void Execute()
	{
		EventManager.Instance.TriggerEvent (new QueryEvent(_Query));
	}
}
