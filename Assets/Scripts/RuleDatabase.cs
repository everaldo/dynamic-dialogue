﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class RuleDatabase : MonoBehaviour
{
	public List<Rule> Rules;

	void Start()
	{
		EventManager.Instance.AddListener<QueryEvent> (Matching);
	}

	void Matching(QueryEvent q)
	{
		RuleResult BiggestMatchResult = null;
		foreach (Rule rule in Rules) 
		{
			RuleResult tempResult = rule.Matching (q.KeyPair);
			if(tempResult.IsMatched && (BiggestMatchResult == null || BiggestMatchResult.NumOfMatchedCriterion <= tempResult.NumOfMatchedCriterion))
				BiggestMatchResult = tempResult;
		}

		if (BiggestMatchResult != null) 
		{
			if (q.KeyPair.ContainsKey("Who")) {
				foreach (Memory fact in BiggestMatchResult.Facts)
					EventManager.Instance.TriggerEvent (new AddMemoryEvent (q.KeyPair["Who"], fact.Name, fact.Value));
				EventManager.Instance.TriggerEvent (new SearchResponseEvent(BiggestMatchResult.ResponseName, q.KeyPair["Who"]));
			}

		}
	}
}

[System.Serializable]
public class Rule{
	public string Name;
	public List<CriterionStatic> Criterions;
	public List<Memory> ApplyFacts;
	public string ResponseName;

	public RuleResult Matching(Dictionary<string, float> KeyPair)
	{
		RuleResult result = new RuleResult();
		result.ResponseName = ResponseName;
		result.Facts = ApplyFacts;

		foreach (CriterionStatic criterion in Criterions) 
		{
			float keyValue = 0f;
			if (KeyPair.ContainsKey (criterion.Criterion))
				keyValue = KeyPair [criterion.Criterion];

			if (criterion.Compare(keyValue)) {
				result.NumOfMatchedCriterion += 1;	
			}
		}

		if (result.NumOfMatchedCriterion == Criterions.Count)
			result.IsMatched = true;

		return result;
	}
}

[System.Serializable]
public class RuleResult
{
	public string ResponseName;
	public bool IsMatched;
	public int NumOfMatchedCriterion;
	public List<Memory> Facts;

	public RuleResult()
	{
		IsMatched = false;
		NumOfMatchedCriterion = 0;
	}
}