﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StoredContent : MonoBehaviour {
	public WorldObject Character;
	public Dictionary<string, float> Memories;

	public List<Memory> InitialMemory;
	void Start()
	{
		Memories = new Dictionary<string, float> ();
		foreach(Memory memory in InitialMemory)
			Memories.Add (memory.Name, memory.Value);
		
		EventManager.Instance.AddListener<AddMemoryEvent> (NewMemoryEvent);
	}

	void NewMemoryEvent(AddMemoryEvent e)
	{
		if (Character._HashedName <= e.CharacterName && e.CharacterName <= Character._HashedName) 
		{
			NewMemory (e.Key, e.Value);
		}
	}

	public void NewMemory(string key, float value)
	{
		if (Memories.ContainsKey (key))
			Memories [key] = value;
		else
			Memories.Add (key, value);
	}

	public void RemoveMemory(string key, float value)
	{
		if (Memories.ContainsKey (key) && Memories[key] == value)
			Memories.Remove (key);
	}
}
