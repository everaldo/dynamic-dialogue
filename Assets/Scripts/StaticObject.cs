﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StaticObject : WorldObject {

	override public List<Memory> GetInfo()
	{
		List<Memory> objectMemory = new List<Memory> ();
		objectMemory.Add (new Memory("What", _HashedName));
		return objectMemory;
	}
}
